#!/usr/bin/env python3

import binascii
import socket
import ssl
import threading

def send_message(dns, query, ca_path):
    '''
        Initialize Socket, Wrap Socket, send message

        - Create a new socket using the given address family, socket type
        - The helper functions create_default_context() returns a new context with secure default settings
    '''
    server = (dns, 853)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(80)
    
    # This module provides access to Transport Layer Security
    # wrap_socket() is deprecated no support for server name indication (SNI) 
    context = ssl.create_default_context()
    context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
    context.verify_mode = ssl.CERT_REQUIRED
    context.check_hostname = True
    context.load_verify_locations(ca_path)

    wrapped_socket = context.wrap_socket(sock, server_hostname=dns)
    wrapped_socket.connect(server)
    print(f'Server peer certificate: {str(wrapped_socket.getpeercert())}')

    tcp_msg = "\x00".encode() + chr(len(query)).encode() + query

    print(f'Client request: {str(tcp_msg)}')
    wrapped_socket.send(tcp_msg)
    data = wrapped_socket.recv(1024)
    return data


def thread(data, address, socket, dns, ca_path):

    answer = send_message(dns, data, ca_path)
    if answer:
        print(f'Server reply: {str(answer)}')
        rcode = binascii.hexlify(answer[:6]).decode("utf-8")
        rcode = rcode[11:]
        if int(rcode, 16) == 1:
            print(f'Error processing the request, RCODE = {rcode}')
        else:
            print(f'"Proxy OK, RCODE =  {rcode}')
            return_ans = answer[2:]
            socket.sendto(return_ans, address)
    else:
        print("Empty reply from server.")


def main():
    port = 53
    host = "0.0.0.0"
    dns = "1.1.1.1"
    ca_path = "/etc/ssl/cert.pem"
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind((host, port))
        while True:
            # method Python's socket class, reads a number of bytes sent from an UDP socket.
            data, address = sock.recvfrom(4096)

            '''
                target: target is the callable object to be invoked by the run() method. 
            '''
            threading.Thread(
                target=thread, args=(data, address, sock, dns, ca_path)
            ).start()
    except Exception as e:
        print(e)
    finally:
        sock.close()


if __name__ == "__main__":
    main()
