FROM python:alpine3.14

RUN apk update \
  && apk upgrade \
  && rm -rf /var/cache/apk/*

WORKDIR /usr/src/app
COPY ./dnsproxy.py .

ENTRYPOINT [ "python", "./dnsproxy.py" ]
