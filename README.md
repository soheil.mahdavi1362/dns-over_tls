## How to run project

1- `git clone https://gitlab.com/soheil.mahdavi1362/dns-over_tls.git`  
2. `chmod 774 setup.sh`  
3. `./setup.sh`  

Image **"dns-tls-img"** will be created.  
Then the container with the name **"dns-tls"** will be run.  
Open another terminal and run a querying DNS tool like **"dig"**  

- dig @127.0.0.1 google.com

## Output  
**After running "dig" command you can check output on wireshark: ** 

![](wireshark.png)

**Output of DNS cloudflare ** 

```
; <<>> DiG 9.16.1-Ubuntu <<>> @127.0.0.1 google.com
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 53791
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; PAD: (409 bytes)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             186     IN      A       142.250.74.206

;; Query time: 69 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Sun May 01 18:00:14 CEST 2022
;; MSG SIZE  rcvd: 468
```



## Implementation
The application binds a datagram type socket to the host.  Upon receiving a query the program starts a new thread to process it.
It begins to wrap a new TCP socket using **PROTOCOL_TLS_CLIENT** type SSL and then verify the certificate over port 853. The full message is formatted and sent to the server over an encrypted connection.  

## Security concerns

1. Docker containers are not highly isolated! 
2. Proper Openssl keys and signed certificates  

## Improvements
There a lot improvements that it would be add  
- parameterizing,  Port, Address, DNS, CA
- Utilizing logger
- Implementing this in a **microservice** makes it highly available and scalable
- Block IPs : too much requests from a specific ip address should be block.  
- reducing overhead by eliminating repetitive IP address.  
- Caching  

